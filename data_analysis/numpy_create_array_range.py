#!/usr/bin/env python3
import numpy as np

# With 'arange', we can create arrays just like we created lists with 'range'
# This gives us an array ranging from the numbers in the arguments

# np.arange(0,12)
r = np.arange(0, 12)

print(r)

# Want a blank array? Create it full of zeros with 'zeros'
# The argument within it create the shape of a 2d or 3d array

# np.zeros((3,11))
s = np.zeros((3, 11))

print(s)

# Hate zeros? Why not use 'ones'?!

# np.ones((3,11))
t = np.ones((3, 11))

print(t)

# Creating dummy data or need a random number?
# randint and randn are useful here

# Creates random numbers around a standard distribution from 0
# The argument gives us the array's shape
print(np.random.randn(3, 3))

# Creates random numbers between two numbers that we give it
# The third argument gives us the shape of the array
print(np.random.randint(1, 100, (3, 3)))
