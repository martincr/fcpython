#!/usr/bin/env python3
import numpy as np

# Three lists, one for GK heights, one for GK weights, one for names
# Create an array with each list

GKNames = ["Kaller", "Fradeel", "Hayward", "Honeyman"]
GKHeights = [184, 188, 191, 193]
GKWeights = [81, 85, 103, 99]

np.array(GKNames)
GKHeights = np.array(GKHeights)
np.array(GKWeights)

# What is the largest height, .max()?

t = GKHeights.max()
print(t)

# What location is the max, .argmax()?

u = GKHeights.argmax()
print(u)

# Can I use this method to locate the player's name?
# Instead of a number in the square brackets, I can just put this method

# GKNames[GKHeights.argmax()]
v = GKNames[3]
print(v)
